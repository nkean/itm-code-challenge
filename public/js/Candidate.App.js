const app = angular.module("Candidate.App", []);

app.component("itmRoot", {
    controller: class {
        constructor() {
            this.candidates = [{ name: "Puppies", votes: 11, percent: 37 }, { name: "Kittens", votes: 12, percent: 40 }, { name: "Gerbils", votes: 7, percent: 23 }];
            this.totalVotes = 30;
        }

        onVote(candidate) {
            console.log(`Vote for ${candidate.name}`);
            this.totalVotes++;
            candidate.votes++;
            this.calcVotePercentages();
        }

        onAddCandidate(candidate) {
            const newCandidate = {
                name: candidate.name,
                votes: 0,
                percent: 0
            };
            if(this.candidates.some(candidate => candidate.name === newCandidate.name)) {
                alert(`${newCandidate.name} is already in the race. Cast your vote to show your support!`);
            }
            else if(newCandidate.name === '' | undefined) {
                alert(`Please enter a name to enroll a candidate in the race.`);
            }
            else {
                console.log(`Added candidate ${candidate.name}`);
                this.candidates.push(newCandidate);
            }
        }

        onRemoveCandidate(candidate) {
            const removeCandidate = candidate;
            const candidateIndex = this.candidates.map(candidate => candidate.name).indexOf(removeCandidate.name);
            if(confirm(`Are you sure you want to remove ${removeCandidate.name} from the race?`)) {
                this.totalVotes -= removeCandidate.votes;
                this.candidates.splice(candidateIndex, 1);
                this.calcVotePercentages();
                console.log(`Removed candidate ${candidate.name}`);
            }
        }

        calcVotePercentages() {
            this.candidates.forEach( candidate => {
                candidate.percent = Math.round((candidate.votes / this.totalVotes) * 100);
            });
        }
    },
    template: `
        <h1 class="text-center">Which candidate brings the most joy?</h1>
             
        <itm-results 
            candidates="$ctrl.candidates"
            on-remove="$ctrl.onRemoveCandidate($candidate)"
            on-vote="$ctrl.onVote($candidate)">
        </itm-results>

        <itm-management 
            candidates="$ctrl.candidates"
            on-add="$ctrl.onAddCandidate($candidate)">
        </itm-management>
    `
});

app.component("itmManagement", {
    bindings: {
        candidates: "<",
        onAdd: "&"
    },
    controller: class {
        constructor() {
            this.newCandidate = {
                name: ""
            };
        }

        submitCandidate(candidate) {
            this.onAdd({ $candidate: candidate });
            this.newCandidate.name = "";
        }
    },
    template: `
        <h3>Add New Candidate</h3>
        <form ng-submit="$ctrl.submitCandidate($ctrl.newCandidate)" novalidate>

            <div class="form-group">
                <input type="text" 
                    ng-model="$ctrl.newCandidate.name" 
                    placeholder="Candidate Name"
                    class="form-control" required>
            </div>
            <button type="submit" class="btn btn-success">Add</button>
        </form>
    `
});

app.component("itmResults", {
    bindings: {
        candidates: "<",
        onRemove: "&",
        onVote: "&"
    },
    controller: class {
        removeCandidate(candidate) {
            this.onRemove({ $candidate: candidate });
        }
    },
    template: `
        <h2>Live Results</h2>
        <div class="d-flex flex-wrap">
            <div class="card"
                ng-repeat="candidate in $ctrl.candidates | orderBy: '-votes' "
            >
                <div class="card-header text-center">
                    <h4>{{candidate.name}}</h4>
                </div>
                <div class="card-body">
                    <p>Votes for Candidate: {{candidate.votes}}</p>
                    <p>Percent of Votes: {{candidate.percent}}%</p>
                </div>
                <div class="card-footer">
                    <button type="button" 
                        class="btn btn-danger btn-sm float-left"
                        ng-click="$ctrl.removeCandidate(candidate)">
                        <span>Remove</span>
                    </button>
                    <button type="button" 
                        class="btn btn-primary btn-sm float-right"
                        ng-click="$ctrl.onVote({ $candidate: candidate })">
                        <span>Cast Vote</span>
                    </button>
                </div>
            </div>
        </div>
    `
});
